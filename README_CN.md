[English](./README.md) | 简体中文 | [Changelogs](./ChangeLogs_EN.md) | [变更日志](ChangeLogs_CN.md) | [Instructions-说明书](./Instructions-说明书.md) | [加入我们](./Join-Us.md)


# wMath
========================================
## 网站：

- [下载](https://api.wyjs.fun/)
- [使用wMath搭建的网站](https://wmath-example.wyjs.fun)

## 联系:
- 加入我们:join-us@wyjs.fun
- 反馈及Bugs:feedbacks@wyjs.fun

## 特点:

  - 不需要把计算过程写在你的js文件里，交给wMath就行了！
  - 使用的时候，只要调用对应的对象，传入参数就可以得到结果！

## 使用:

选择1: 
  - <script src="https://raw.githack.com/Wuyingqwq/wMath/main/Builds/Latest/wMath-Default-1.3.9.js"> </script> 
  - <script src="http://wyjs.fun/wmath/algebra-0.2.4.min.js"> </script> 
  - 如果你是一个开发者，可以使用下面这个版本：
  - <script src="https://raw.githack.com/Wuyingqwq/wMath/main/Builds/Latest/wMath-Debug-1.3.9.js"> </script> 
  - <script src="http://wyjs.fun/wmath/algebra-0.2.4.min.js"> </script> 

选择2: 

  - 步骤1 下载wMath.js 

  - 步骤2 使用<script>标签引入wMath.js
  

  
## 注意事项：
  - 使用 eq 对象时，必须引入 Algebra.js。
  - Algebra.js:https://github.com/nicolewhite/algebra.js
