English | [变更日志（简体中文）](./ChangeLogs_CN.md) | [Instructions-说明书](./Instructions-说明书.md)
# V1.3.9：
## Fix the bug that wMath's announcement cannot be used when your website is HTTPS protocol

V1.3.8:
- Add judgment IP automatic switching announcement language

V1.3.7:
- Improved version detection and added announcements 

V1.3.6:
- Add "complementAngle" and "supplementaryAngle" functions

V1.3.5F:
- Optimized version detection

V1.3.5:
- Added version detection 

V1.3.4:
- Code for the confusion
- Fix bugs

V1.3.3:
- Add "availWidth" and "availHeight" in wMath_tools
- Add load complete message 
- Fix bugs

V1.3.1:
- Add "now" function in wMath_tools
- X Close wMath's website
- [010622]wMath's website is back

V1.3:
- Add wMath_tools
- Improve AMNO function

V1.2.5:
- Delete algebra-0.2.4.min.js,now you must introduce Algebra.js again
- Fix bugs in AMNO function

V1.2.4:
- Add algebra-0.2.4.min.js

V1.2.3:
- Add getValue function
- Add changeColor function
- Add changeValue function
- Add when CMMNO and AMNO functions the return value is infinite will return alert message
- Fix bugs and completion of comments


V1.2.2:
- Fix Supplementary notes.

 V1.2.1:
- Add multiple numbers.
- Add consecutive numbers.
- / Fix bugs.

V1.2:
+ Add multiple numbers.
+ Multiply multiple numbers.
+ Add consecutive numbers.
+ Multiply consecutive numbers.
/ Fix bugs.

Beta 091121:Add When use "GetThisTime" Object Will Relaod Html.

Beta 091121:Improve "GetThisTime" Object.

Beta 091021:Add "GetThisTime" Object.







