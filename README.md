English | [简体中文](./README_CN.md) | [Changelogs](./ChangeLogs_EN.md) | [变更日志（简体中文）](ChangeLogs_CN.md) | [Instructions-说明书](./Instructions-说明书.md) | [Join us](./Join-Us.md)

# wMath
========================================
## Website:

- [Download](https://api.wyjs.fun/)
- [Website built using wMath](https://wmath-example.wyjs.fun/)

## Connect:
 - Join us:join-us@wyjs.fun
 - Feedbacks & Bugs:feedbacks@wyjs.fun

## Features:

  - Don't need write the calculation process in your js file.Just leave it to wMath!
  - When you use it, just call the corresponding object and pass the parameters to get the result!

## Usage:

Choose one: 
  - <script src="https://raw.githack.com/Wuyingqwq/wMath/main/Builds/Latest/wMath-Default-1.3.9.js"> </script> 
  - <script scr="https://raw.githack.com/Wuyingqwq/wMath/main/Builds/Other-js/algebra-0.2.4.min.js"></script>
  + If you are a developer you can use this version:
  - <script src="https://raw.githack.com/Wuyingqwq/wMath/main/Builds/Latest/wMath-Debug-1.3.9.js"> </script>
  - <script scr="https://raw.githack.com/Wuyingqwq/wMath/main/Builds/Other-js/algebra-0.2.4.min.js"></script> 

Choose two: 

  - Step.1 Download wMath.js 

  - Step.2 Use <script> label introduce wMath.js
  

  
## Notices:
  - When you use the eq object,you must introduce Algebra.js.
  - Algebra.js:https://github.com/nicolewhite/algebra.js
