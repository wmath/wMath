# wMath:

##       CMMNO:
                -Continuous number multiplication (连续数相乘)
                -Two numbers need to be passed in (需传入两个数字)
                -If result is Infinity it will return "您输入的数字过大！",you can change it (如果结果是Infinity，它会返回"您输入的数字过大！"，您可以更改它)

        
##        AMNO:
                -Continuous number addition (连续数相加)
                -Two numbers need to be passed in (需传入两个数字)
                -If result is Infinity it will return "您输入的数字过大！",you can change it (如果结果是Infinity，它会返回"您输入的数字过大！"，您可以更改它)

##        eq:
                -Solving binary linear equations (解二元一次方程)
                -When you use this function you must Introduction of algebra js (当你使用这个函数时，你必须引入Algebra.js)
                -Two strings need to be passed in (需传入两个字符串)
                -The equation of the unknown can be x, y (方程的未知数只能是x，y)
                -You must enter the right formulas,else it will wrong (你必须输入正确的式子，否则就会出错)
                -The return value is an array, [0] for x expression, [1] for y expression, [2] for the value of x, [3] with the value of y (返回值是一个数组，[0]为x的表达式，[1]为y的表达式,[2]为x的值,[3]为y的值)

##         pow:
                -Calculate the y-power of X (计算x的y次幂)
                -Two numbers need to be passed in (需传入两个数字)
##         abs:
                -Gets the absolute value of X (获取x的绝对值)
                -Two numbers need to be passed in (需传入两个数字)
##         max:
                -To obtain the maximum number of x and y (获取x和y的最大值)
                -Two numbers need to be passed in (需传入两个数字)
##         min:
                -The minimum value for x and y (获取x和y的最小值)
                -Two numbers need to be passed in (需传入两个数字)
##         trunc:
                -Use to tail method to integer x (使用去尾法将x取整)
                -Two numbers need to be passed in (需传入两个数字)
##          random:
                -From 0 to 1 more than a random decimal (not including 1) (从0到1随机一个多位小数（不包括1）)   
                Without any incoming parameters (无需传入任何参数)
##          round:
                -Will use a decimal rounded to integer (将一个小数使用四舍五入进行取整)  
                -Need to pass in a number (需要传入一个数字)  
##          proportion:
                -Determine whether two than in proportion (判断两个比是否成比例)
                -Need to two than four Numbers (需要传入两个比即四个数字)
                -W, and x is the first, y, and z is than the second (w和x是第一个比，y和z是第二个比) 
##          thisYear:
                -Whether this year is a leap year (判断今年是否为闰年)
                -Without any incoming parameters (无需传入任何参数)
##          year:
                -Decide whether a given year is a leap year (判断指定年份是否为闰年)    
                -Need to pass in a number (需要传入一个数字) 
##          fraction:
                -Determine the size of the two points (判断两个分数的大小)   
                -If the first score big, return true, otherwise it returns false (如果第一个分数大，则返回true，否则返回false)
                -W is the first to score, x is the first to score the denominator, y is the second grade, z is the second fraction molecules (w是第一个分数的分子，x是第一个分数的分母，y是第二个分数的分子，z是第二个分数的分子)    
                -Need to two score four Numbers (需要传入两个分数即四个数字)                             
##          randomFromXY:
                -From x to y random access to more than one decimal (从x到y随机获取一个多位小数)
                -X > the counting > = y (x>此得数<=y)
                -Two numbers need to be passed in (需传入两个数字)
##          randomFromXYForInteger:
                -From x to y random access to an integer (从x到y随机获取一个整数)
                -X > the counting < y (x>此得数<y)
                --Two numbers need to be passed in (需传入两个数字)
##          randomFromXYForIntegerincludeXY:
                -From x to y random access to an integer (including the x, y) (从x到y随机获取一个整数（包括x，y）)
                -X => the counting =< y (x>此得数=<y)
                -Two numbers need to be passed in (需传入两个数字)
##          ceil:
                -Method of use into a decimal integer (rounded up) (使用进一法对小数进行取整（向上取整）)
                -One numbers need to be passed in (需传入一个数字)
##          floor:
                -Down to a decimal integer (将一个小数向下取整)
                -One numbers need to be passed in (需传入一个数字)
##          sign:
                -Judgment is negative (判断正负数)
                -If it is positive returns true, if is negative returns false (如果是正数返回true，如果是负数则返回false)
                -One numbers need to be passed in (需传入一个数字)
##          oppositeNumber:
                -Get the opposite of x (获取x的相反数)
                -One numbers need to be passed in (需传入一个数字)
                
##          complementAngle:
                -To get a complementary angle (获取一个角的余角)
                -One angle need to be passed in (需要传入一个角的度数)

##          supplementaryAngle:
                -To get a supplementary angle (获取一个角的补角)
                -One angle need to be passed in (需要传入一个角的度数)
                



# getDate:
            -In the use of wMath_tools is used, please do not delete (在使用wMath_tools是会用到，请勿删除)



# wMath_tools:

##          getWeek:
                -What day is it today (获取今天星期几)  
                -You can change its return values (你可以改变它的返回值)  
                -Without any incoming parameters (无需传入任何参数)    
##          getMonth:
                -For the current month (获取当前月份)    
                -It will return the current month (它会返回月份)  
                -Without any incoming parameters (无需传入任何参数)
##          getYear:
                -Gets the current year (获取当前年份)
                -It will return the current year (它将会返回年份)
                -Without any incoming parameters (无需传入任何参数)
##          getDate:
                -Gets the current day (获取当前几号)
                -It will return the current day (它将会返回几号)
                -Without any incoming parameters (无需传入任何参数)
##          getHours:
                -Gets the current hours (获取当前的小时)
                -It will return the current hours (它将会返回小时)
                -Without any incoming parameters (无需传入任何参数)
##          getMinutes:
                -Gets the current minutes (获取当前的分钟)
                -It will return the current minutes (它将会返回分钟)
                -Without any incoming parameters (无需传入任何参数)
##          getSeconds:
                -Gets the current seconds (获取当前的秒数)
                -It will return the current seconds (它将会返回秒数)
                -Without any incoming parameters (无需传入任何参数)
##          getThisTime:
                -Gets the current time (获取当前时间)
                -It will return the the current time (它将会返回当前时间)
                -You need to "The current time is:","Years", "Months", "Days" and "Hours", "Minutes", "Seconds" (你需要传入“当前时间是：”，“年”，“月”，“日”，“时”，“分”，“秒”)
##          now:
                -Loop to get the current time (循环获取当前时间)
                -Default is cycle for seven days (默认是循环七天)
                -You can change its output (你可以更改它的输出方式)
                -Without any incoming parameters (无需传入任何参数)
##          availWidth:
                -To obtain the available width of the screen (获取屏幕的可用宽度)
                -Without any incoming parameters (无需传入任何参数)
##          availHeight:
                -To obtain the available height of the screen (获取屏幕的可用高度)
                -Without any incoming parameters (无需传入任何参数)



# wMath_html:

##          $:
                -Omit "document. The getElementById (id)," like jQuery (省略"document.getElementById(id)",像jQuery一样)
                -For example,“wMath_html.$('test').style.color = 'red'” (如：“wMath_html.$('test').style.color = 'red'”)
                -Need to pass in a id (需要传入一个id)
##          getValue:
                -To obtain the value of an element (获取某个元素的内容)
                -It returns an element's content (它会返回某个元素的内容)
                -Need to pass in a id (需要传入一个id)
##          changeColor:
                -Change the color an element (更改某个元素的颜色)
                -Need to pass in a id and need to change the color (需要传入一个id和需要改变的颜色)
##          changeValue:
                -Change the value an element (更改某个元素的内容)
                -Need to pass in a id and to change the content (需要传入一个id和要改变的内容)



# Notes:
##          algebra.js:
                -https://github.com/nicolewhite/algebra.js  
##          Functions:
                -If you input is not number, it will return to "Undefined" in some of the features (如果你输入的不是数字，在某些功能中它会返回"Undefined")
                -If it returns "you input number is too large!", indicating that the result is too large, you can change this value (如果它返回“您输入的数字过大！”，说明结果过于大，您可以更改这个值)     



# About:
##          Made by wuying.
##          Email:feedbacks@wyjs.fun
##          Website:https://api.wyjs.fun/
##          Github:https://github.com/Wuyingqwq/wMath
