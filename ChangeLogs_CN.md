[English](./ChangeLogs_EN.md) | 变更日志（简体中文） | [Instructions-说明书](./Instructions-说明书.md)
# V1.3.9:
## 修复当你的网站是HTTPS协议时wMath的公告无法使用的bug

1.3.8:
- 添加判断IP自动切换公告语言

V1.3.7:
- 改进了版本检测，添加公告 

V1.3.6:
- 添加了获取余角"complementAngle"和获取补角"supplementaryAngle" 

V1.3.5F:
- 优化了版本检测

V1.3.5:
- 添加了版本检测

V1.3.4:
- 对代码进行了混淆
- 修复bugs

V1.3.3:
- 添加“availWidth”和“availHeight”在wMath_tools
- 添加加载完成时的信息
- 修复Bug

V1.3.1:
- 添加now功能在wMath_tools中
- X 关闭wMath的网站
- [010622]wMath的网站恢复！

V1.3:
- 添加wMath_tools
- 改进AMNO功能

V1.2.5:
- 删除algebra-0.2.4.min.js的代码，现在你必须再次引入Algebra.js
- 修复AMNO功能的bug

V1.2.4:
- 添加algebra-0.2.4.min.js的代码

V1.2.3:
- 添加getValue功能
- 添加changeColor功能
- 添加changeValue功能
- 添加当的CMMNO和AMNO函数返回值为'Infinite'时会返回警告信息
- 修复Bug及补全注释


V1.2.2:
- / 补充注释。

V1.2.1:
- 删除多个数相加。 
- 删除多个数相乘。
- 修复bug。

V1.2:
- 添加多个数相加。
- 添加多个数相乘。
- 添加连续数相加。
- 添加连续数相乘。
- 修复bug。

Beta 091121：
- 添加当使用“GetThisTime”对象时将重新加载Html。

Beta 091121：
- 改进“GetThisTime”对象。

Beta 091021：
- 添加“GetThisTime”对象。







